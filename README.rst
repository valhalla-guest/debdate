=========
 debdate
=========

------------------------------------------------
 Convert Gregorian dates to Debian Regnal dates
------------------------------------------------

``debdate`` is a fundamental tool for anybody who follows the debian
calendar where the years are named after the current stable release.

Installation
------------

``debdate`` depends on python3 and dateutil; no installation is
required, as the script can be run directly from the repository.

Under Debian and possibly some derivatives, ``debdate`` can read
up-to-date release dates from the package distro-info-data.

License
-------

Copyright (C) 2017 Elena Grandi <valhalla@trueelena.org>

This program is free software. It comes without any warranty, to the
extent permitted by applicable law. You can redistribute it and/or
modify it under the terms of the Do What The Fuck You Want To Public
License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.
